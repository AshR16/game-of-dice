import React from 'react'

import { StyleSheet, Text, View} from "react-native";
import { NativeRouter, Route} from "react-router-native";
import Main from './src/Screen/GameScreen';
import PlayerInput from "./src/Screen/PlayerInput"
import PlayerOrder from './src/Screen/PlayerOrder';


export default function App() {
    return (
        <NativeRouter>
        <View style={styles.container}>
          <Route exact path="/" component={PlayerInput} />
          <Route path="/PlayerOrder" component={PlayerOrder} />
          <Route path="/Main" component={Main} />
        </View>
      </NativeRouter>
    )
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',     
      justifyContent: 'center',
     
    },
  });