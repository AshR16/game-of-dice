import React from 'react';
import {StyleSheet} from "react-native"
import { Card, Title, Paragraph } from 'react-native-paper';

const CardContainer = props => (
  <Card style={styles.container}>
    <Card.Content>
      <Title>{props.title}</Title>
    </Card.Content>
  </Card>
);


const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:"pink",
    margin:15
  },
});


export default CardContainer;