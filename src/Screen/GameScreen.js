import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Button,
  FlatList,
  ScrollView,
  StyleSheet,
  Alert,
} from "react-native";

import { Card, Title, Paragraph } from "react-native-paper";
import { useHistory } from "react-router-dom";

export default function Main(props) {
  let history = useHistory();

  let player = props.history.location.state.shuffledPlayer;
  let targetValue = parseInt(props.history.location.state.targetCount);

  const [playerData, setPlayerData] = useState(player);
  const [randomNumber, setRandomNumber] = useState(0);
  const [turns, setTurns] = useState([]);
  const [rank, setRank] = useState(1);

  //console.log(player,"player")

  useEffect(() => {
    const recentTurn = turns[turns.length - 1];

    if (!recentTurn) return;

    /* Iterate over the playerData loop and add the generated random Number to the score of the player
     based on the index */

    const newPlayers = playerData.map((player, index) => {
      if (index === recentTurn.playerIndex && player.score < targetValue) {
        return {
          ...player,
          score: player.score + recentTurn.randomNum,
          scoreTrack: [...player.scoreTrack, recentTurn.randomNum],
          rank: player.rank,
        };
      } else if (index === recentTurn.playerIndex && player.score >= targetValue) {
        setRank((prevRank) => prevRank + 1);
        if (typeof player.score === "number") {
          return {
            ...player,
            score: `Rank ${rank}`,
          };
        }
      }
      return player;
    });

    setPlayerData(newPlayers);
  }, [turns, rank]);


  
  const handleDice = () => {
    const prevTurn = turns[turns.length - 1];
    //console.log(prevTurn,"prevTurn")
    //console.log(turns.length,"turns")
    let sum
    if(prevTurn!=undefined){
      let scoreSum= playerData[prevTurn.playerIndex].scoreTrack
      sum = scoreSum.reduce(((ac,cv)=>ac+cv))
      console.log(scoreSum,"scoreSum")
      console.log(sum,"sum")
    }
    let currentIndex;
    if (!prevTurn) {
      currentIndex = 0;
     
    } else if (prevTurn.randomNum == 6 ||prevTurn.randomNum == 5 ||sum == 10) {
      currentIndex = prevTurn.playerIndex;
      //console.log(playerData[prevTurn.playerIndex].scoreTrack,"checking")
    } else {
      currentIndex = prevTurn.playerIndex + 1;
     // console.log(playerData[prevTurn.playerIndex].scoreTrack,"checking")
    }
    
    // Random number is generated on press button
  // player 1  -> [3,2,4,4,2]==> if 4+4+2 ==10 then get a chance 
  //player 2   -> [1,2,1,2,4]
  

   

   //console.log(playerData,"playerData")
    let randomNumber = Math.floor(Math.random() * 6) + 1;
    setRandomNumber(randomNumber);
    const newTurn = {
      randomNum: randomNumber,
      playerIndex: currentIndex % playerData.length,
    };

    setTurns([...turns, newTurn]);
  };



  // Logic to find player turn 

  let displayTurns;
  if (!turns.length) {
    displayTurns = 0;
  } else {
    displayTurns = playerData[turns[turns.length - 1].playerIndex].id + 1;
  }

  return (
    <View style={styles.container}>
      <Text style={styles.heading}>{randomNumber}</Text>
      <Button title="Roll" onPress={handleDice} />
      <Text style={styles.heading}>Current Player {displayTurns} </Text>
      <ScrollView>
        {playerData.map((player) => (
          <View key={player.id} style={styles.cardView}>
            <Card style={styles.cards}>
              <Card.Content>
                <Title>
                  {player.name}: {player.score}
                </Title>
              </Card.Content>
            </Card>
          </View>
        ))}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "center",
    margin: 50,
  },
  heading: {
    fontSize: 20,
    fontWeight: "bold",
    color: "#1A1F38",
    marginVertical:20
  },
  cards: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "pink",
    margin: 5,
  },
});
