import React, { useState } from "react";
import { View, Text, StyleSheet, TextInput, Button } from "react-native";
import { useHistory } from "react-router-dom";

export default function Input() {
  let history = useHistory();

  const [playerCount, setPlayerCount] = useState("");
  const [targetCount, setTargetCount] = useState("");

  const playerInputHandler = (enteredText) => {
    setPlayerCount(enteredText);
  };

  const targetInputHandler = (enteredText) => {
    setTargetCount(enteredText);
  };

  function handleClick() {
    history.push({
      pathname: "/PlayerOrder",
      state: { playerCount: playerCount, targetCount: targetCount },
    });
  }

  return (
    <View style={styles.container}>
      <View>
        <TextInput
          placeholder="Enter Number of Players"
          style={styles.inputText}
          onChangeText={playerInputHandler}
          value={playerCount}
          keyboardType="numeric"
        />
      </View>
      <View style={styles.holder}>
        <TextInput
          placeholder="Enter the target Value"
          style={styles.inputText}
          onChangeText={targetInputHandler}
          value={targetCount}
          keyboardType="numeric"
        />
      </View>

      <Button title="Submit" onPress={handleClick} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "space-between",
    alignItems: "center",
  },
  inputText: {
    width: "80%",
    borderColor: "grey",
    borderWidth: 2,
    padding: 10,
    margin: 40,
  },
});
