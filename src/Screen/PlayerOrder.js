import React from "react";
import { View, Text, StyleSheet, Button, FlatList } from "react-native";
import { useHistory } from "react-router-dom";
import CardContainer from "../component/CardContainer";

export default function PlayerOrder(props) {
  let history = useHistory();

  let numberOfPlayer = props.history.location.state.playerCount;
  let targetValue = props.history.location.state.targetCount;

  const player = [];
  
  // Generate n number of players based on the input ( n represents numberOfPlayer from PlayerInput)

  for (let i = 0; i < numberOfPlayer; i++) {
    player.push({
      id: i,
      name: `Player ${i + 1}`,
      score: 0,
      scoreTrack: [],
      rank: 0,
    });
  }
  
  // Shuffle the generated n number of players 

  const shuffledPlayer = player.sort(() => Math.random() - 0.5);

  const renderItem = ({ item }) => <CardContainer title={item.name} />;

  function handleClick() {
    history.push({
      pathname: "/Main",
      state: {
        playerCount: numberOfPlayer,
        targetCount: targetValue,
        shuffledPlayer: shuffledPlayer,
      },
    });
  }

  return (
    <View style={styles.container}>
      <Text>Order of the Player !</Text>
      <View style={styles.card}>
        <FlatList
          data={player}
          renderItem={renderItem}
          keyExtractor={(item) => item.id.toString()}
        />
      </View>
      <View style={styles.button}>
        <Button title="start" onPress={handleClick} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: 100,
    alignItems: "center",
    flex: 1,
    width: "80%",
  },
  card: {
    flex: 1,
    height: "15%",
    width: "100%",
  },
  button: {
    flex: 1,
    alignItems: "center",
  },
});
